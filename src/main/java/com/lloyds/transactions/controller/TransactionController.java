package com.lloyds.transactions.controller;

import com.lloyds.transactions.exceptions.TransactionNotFoundException;
import com.lloyds.transactions.model.TransactionRequest;
import com.lloyds.transactions.model.TransactionResponse;
import com.lloyds.transactions.service.TransactionService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

/**
 * Rest Controller for Transaction Entity
 */
@RestController
@RequestMapping("/transactions")
public class TransactionController {

    Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    private TransactionService transactionService;

    /**
     * <p>This method saves the request as Transaction Entity
     * </p>
     * @param transaction Client new Transaction details
     * @return ResponseEntity with location of the new Resource
     */
    @ApiOperation(value = "Create Transaction", notes = "This method saves the request as Transaction Entity")
    @PostMapping(path = "/",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveTransaction(@Valid @RequestBody TransactionRequest transaction) {
        logger.info("TransactionController saveTransaction " + transaction);
        TransactionResponse response = transactionService.save(transaction);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(response.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    /**
     * <p>This method retrieves the Transaction Entity based on the given transaction Id
     * </p>
     * @param id Transaction Id
     * @return Client Transaction
     */
    @ApiOperation(value = "Find Transaction by Id", notes = "Retrieves Client Transaction Entity based on the given transaction Id")
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionResponse> getTransaction(@PathVariable("id") Long id) {
        logger.info("TransactionController getTransaction " + id);
        Optional<TransactionResponse> transaction = transactionService.findById(id);
        if (!transaction.isPresent())
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

        return ResponseEntity.status(HttpStatus.OK).body(transaction.get());
    }

    /**
     * <p>This method updates the Transaction Entity based on the given request and transaction Id
     * </p>
     * @param id Transaction Id
     * @param transaction Updated Transaction Details
     * @return Response Code
     */
    @ApiOperation(value = "Update Transaction by Id", notes = "Updates Client Transaction Entity based on the given transaction Id")
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateTransaction(@PathVariable("id") Long id, @Valid @RequestBody TransactionRequest transaction) {
        logger.info("TransactionController updateTransaction " + id + " "+transaction);
        try {
            transactionService.updateById(id, transaction);
        } catch (TransactionNotFoundException ee) {
            //Log Exception
            logger.error(" updateTransaction " + ee.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    /**
     * <p>This method deletes the Transaction Entity based on the given transaction Id
     * </p>
     * @param id Transaction Id
     * @return Response Code
     */
    @ApiOperation(value = "Delete Transaction by Id", notes = "Deletes Client Transaction Entity based on the given transaction Id")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity deleteTransaction(@PathVariable("id") Long id) {
        logger.info("TransactionController deleteTransaction " + id);
        try {
            transactionService.deleteById(id);
        } catch (TransactionNotFoundException ee) {
            //Log Exception
            logger.error(" deleteTransaction " + ee.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
