package com.lloyds.transactions.service;

import com.lloyds.transactions.domain.Transaction;
import com.lloyds.transactions.exceptions.TransactionNotFoundException;
import com.lloyds.transactions.model.TransactionRequest;
import com.lloyds.transactions.model.TransactionResponse;
import com.lloyds.transactions.repository.TransactionRepository;
import com.lloyds.transactions.util.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Test cases for TransactionService
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class TransactionServiceTest {

    @InjectMocks
    private  TransactionService transactionService;

    @Mock
    private TransactionRepository transactionRepository;

    private ModelMapper modelMapper;

    @Before
    public void setUp(){
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT);
        transactionService.setModelMapper(modelMapper);
    }

    @Test
    public void whenSaveTransaction_thenNoException(){
        //Given
        TransactionRequest transactionRequest = TestUtil.getMockTransactionRequestStatic();
        Transaction transaction = TestUtil.getMockTransactionStatic();
        when(transactionRepository.save(any(Transaction.class))).thenReturn(transaction);
        //When
        TransactionResponse response = transactionService.save(transactionRequest);
        //then
        assertNotNull(response);
    }

    @Test
    public void whenGetTransaction_thenNoException(){
        //Given
        Transaction transaction = TestUtil.getMockTransactionStatic();
        when(transactionRepository.findById(any(Long.class))).thenReturn(Optional.of(transaction));
        //When
        Optional<TransactionResponse> response = transactionService.findById(1L);
        //then
        assertNotNull(response);
    }

    @Test
    public void whenUpdateTransaction_thenNoException () throws TransactionNotFoundException{
        //Given
        TransactionRequest transactionRequest = TestUtil.getMockTransactionRequestStatic();
        Transaction transaction = TestUtil.getMockTransactionStatic();
        when(transactionRepository.findById(any(Long.class))).thenReturn(Optional.of(transaction));
        //When
        transactionService.updateById(1L, transactionRequest);
        //then throw no exception
        //check repo method being called
        verify(transactionRepository, times(1)).findById(any());
        verify(transactionRepository, times(1)).save(any());

    }

    @Test
    public void whenDeleteTransaction_thenNoException() throws TransactionNotFoundException {
        //Given
        long transactionId = 1;
        Transaction transaction = TestUtil.getMockTransactionStatic();
        when(transactionRepository.findById(any(Long.class))).thenReturn(Optional.of(transaction));
        //When
        transactionService.deleteById(transactionId);
        //then throw no exception
        //check repo method being called
        verify(transactionRepository, times(1)).findById(any());
        verify(transactionRepository, times(1)).deleteById(any());
    }

}
