package com.lloyds.transactions.util;

import com.lloyds.transactions.domain.Transaction;
import com.lloyds.transactions.model.TransactionRequest;
import com.lloyds.transactions.model.TransactionResponse;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class TestUtil {


    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    public static TransactionRequest getMockTransactionRequestStatic(){
        return TestUtil.getMockTransactionRequest("2020-10-22T18:59:33+0000",
                "1234-5678-9012-1234", "9d078bd3-ec13-4728-b484-8dbc2191c32c",
                "200.00");
    }

    public static TransactionRequest getMockTransactionRequest(String transactionDate, String cardNumber,
                                                               String clientId, String transactionAmount){
        TransactionRequest transaction = new TransactionRequest();
        transaction.setTransactionDate(OffsetDateTime.parse(transactionDate, formatter));
        transaction.setCardNumber(cardNumber);
        transaction.setClientId(clientId);
        transaction.setTransactionAmount(new BigDecimal(transactionAmount));
        return transaction;
    }

    public static TransactionResponse getMockTransactionResponseStatic(){
        return TestUtil.getMockTransactionResponse("2020-10-22T18:59:33+0000",
                "1234-5678-9012-1234", "9d078bd3-ec13-4728-b484-8dbc2191c32c",
                "200.00", 1L);
    }

    public static TransactionResponse getMockTransactionResponse(String transactionDate, String cardNumber,
                                                                String clientId, String transactionAmount, Long id){
        TransactionResponse transaction = new TransactionResponse();
        transaction.setId(id);
        transaction.setTransactionDate(OffsetDateTime.parse(transactionDate, formatter));
        transaction.setCardNumber(cardNumber);
        transaction.setClientId(clientId);
        transaction.setTransactionAmount(new BigDecimal(transactionAmount));
        return transaction;
    }

    public static Transaction getMockTransactionStatic(){
        return TestUtil.getMockTransaction("2020-10-22T18:59:33+0000",
                "1234-5678-9012-1234", "9d078bd3-ec13-4728-b484-8dbc2191c32c",
                "200.00", 1L);
    }

    public static Transaction getMockTransaction(String transactionDate, String cardNumber,
                                                                 String clientId, String transactionAmount, Long id){
        Transaction transaction = new Transaction();
        transaction.setId(id);
        transaction.setTransactionDate(OffsetDateTime.parse(transactionDate, formatter));
        transaction.setCardNumber(cardNumber);
        transaction.setClientId(clientId);
        transaction.setTransactionAmount(new BigDecimal(transactionAmount));
        return transaction;
    }


}
