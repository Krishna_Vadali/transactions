package com.lloyds.transactions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lloyds.transactions.domain.Transaction;
import com.lloyds.transactions.model.TransactionRequest;
import com.lloyds.transactions.model.TransactionResponse;
import com.lloyds.transactions.repository.TransactionRepository;
import com.lloyds.transactions.util.TestUtil;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.event.annotation.BeforeTestMethod;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TransactionsApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private TransactionRepository transactionRepository;

	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

	@Test
	public void whenPost_withValidRequest_thenReturns201() throws Exception{
		TransactionRequest transactionRequest = TestUtil.getMockTransactionRequestStatic();
		String request = mapper.writeValueAsString(transactionRequest);

		MvcResult resultPost = mockMvc.perform( MockMvcRequestBuilders
				.post("/transactions/")
				.content(request)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
                .andDo(print())
				.andExpect(status().isCreated())
				.andReturn();
		String location = resultPost.getResponse().getHeader("Location");
		Pattern pattern = Pattern.compile("(\\d+)$");
		Matcher matcher = pattern.matcher(location);
		matcher.find();
		Long id = Long.parseLong(matcher.group(), 10);

		Optional<Transaction> transactionOptional =  transactionRepository.findById(id);
		if(transactionOptional.isPresent()){
			Transaction transaction = transactionOptional.get();
			assertThat(transaction.getClientId()).isEqualTo(transactionRequest.getClientId());
			assertThat(transaction.getCardNumber()).isEqualTo(transactionRequest.getCardNumber());
			assertThat(transaction.getTransactionAmount()).isEqualTo(transactionRequest.getTransactionAmount());
			assertThat(transaction.getTransactionDate()).isEqualTo(transactionRequest.getTransactionDate());
		}else{
			Assert.fail();
		}
	}

	@Test
	void whenGet_withValidRequest_thenReturns200() throws Exception {

		Transaction transaction = TestUtil.getMockTransactionStatic();
		transactionRepository.save(transaction);

		String trDate = formatter.format(transaction.getTransactionDate());

		mockMvc.perform( MockMvcRequestBuilders
				.get("/transactions/{id}", 1)
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.clientId").value(transaction.getClientId()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.cardNumber").value(transaction.getCardNumber()))
				//fix scale issue
				//.andExpect(MockMvcResultMatchers.jsonPath("$.transactionAmount").value(transaction.getTransactionAmount()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.transactionDate").value(trDate));
	}

}
