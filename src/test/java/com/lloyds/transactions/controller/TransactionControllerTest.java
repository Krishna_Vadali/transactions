package com.lloyds.transactions.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lloyds.transactions.model.TransactionRequest;
import com.lloyds.transactions.model.TransactionResponse;
import com.lloyds.transactions.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test cases for Transaction Controller
 */
@WebMvcTest(controllers = TransactionController.class)
public class TransactionControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    @MockBean
    private TransactionService userService;

    @Test
    public void whenPost_withValidRequest_thenReturns201() throws Exception{
        TransactionRequest transaction = new TransactionRequest();
        transaction.setTransactionDate(OffsetDateTime.parse("2020-09-22T18:59:33+0000", formatter));
        transaction.setCardNumber("1234-5678-9012-1234");
        transaction.setClientId("9d078bd3-ec13-4728-b484-8dbc2191c32c");
        transaction.setTransactionAmount(BigDecimal.ONE);

        TransactionResponse response = new TransactionResponse();
        response.setId(1L);
        response.setTransactionDate(OffsetDateTime.parse("2020-09-22T18:59:33+0000", formatter));
        response.setCardNumber("1234-5678-9012-1234");
        response.setClientId("9d078bd3-ec13-4728-b484-8dbc2191c32c");
        response.setTransactionAmount(BigDecimal.ONE);

        when(userService.save(any())).thenReturn(response);

        String request = mapper.writeValueAsString(transaction);

        mockMvc.perform( MockMvcRequestBuilders
                .post("/transactions/")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void whenGet_withValidRequest_thenReturns200() throws Exception {

        TransactionResponse response = new TransactionResponse();
        response.setId(1L);
        response.setTransactionDate(OffsetDateTime.parse("2020-09-22T18:59:33+0000", formatter));
        response.setCardNumber("1234-5678-9012-1234");
        response.setClientId("9d078bd3-ec13-4728-b484-8dbc2191c32c");
        response.setTransactionAmount(BigDecimal.ONE);

        when(userService.findById(any())).thenReturn(Optional.of(response));

        mockMvc.perform( MockMvcRequestBuilders
                .get("/transactions/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void whenPut_withValidRequest_thenReturns200() throws Exception{

        TransactionRequest transaction = new TransactionRequest();
        transaction.setTransactionDate(OffsetDateTime.parse("2020-10-22T18:59:33+0000", formatter));
        transaction.setCardNumber("1234-5678-9012-1234");
        transaction.setClientId("9d078bd3-ec13-4728-b484-8dbc2191c32c");
        transaction.setTransactionAmount(BigDecimal.TEN);

        String request = mapper.writeValueAsString(transaction);

        mockMvc.perform( MockMvcRequestBuilders
                .put("/transactions/{id}", 1)
                .content(request)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    void whenDelete_withValidRequest_thenReturns200() throws Exception {
        mockMvc.perform(delete("/transactions/{id}", 1))
                .andExpect(status().isOk());
    }

}
