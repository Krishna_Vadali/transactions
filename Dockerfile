# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine

# Add Maintainer Info
LABEL maintainer="kishore.vadali@gmail.com"

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8081 available to the world outside this container
EXPOSE 8081

# Add the application's jar to the container
ADD target/transactions-0.0.1-SNAPSHOT.jar transactions.jar

COPY src/main/resources/logback-spring.xml logback-spring.xml

# Run the jar file
ENTRYPOINT ["java","-jar","-Dlogging.config=/logback-spring.xml" , "/transactions.jar"]
