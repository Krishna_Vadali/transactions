package com.lloyds.transactions.service;

import com.lloyds.transactions.domain.Transaction;
import com.lloyds.transactions.exceptions.TransactionNotFoundException;
import com.lloyds.transactions.model.TransactionRequest;
import com.lloyds.transactions.model.TransactionResponse;
import com.lloyds.transactions.repository.TransactionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 *  Handles CRUD operations for Transaction Entity
 */
@Service
public class TransactionService{

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * <p>Gets the Transaction entity by the given Id
     * </p>
     * @param id Transaction Id
     * @return TransactionResponse
     */
    public Optional<TransactionResponse> findById(Long id){
        TransactionResponse response = null;
        Optional<Transaction> transactionOptional = transactionRepository.findById(id);
        if(transactionOptional.isPresent()){
            response = modelMapper.map(transactionOptional.get(), TransactionResponse.class);
        }
        return Optional.ofNullable(response);
    }

    /**
     * <p>Persists the given TransactionRequest as Transaction Entity
     * </p>
     * @param transactionRequest new Transaction details
     * @return TransactionResponse
     */
    public TransactionResponse save(TransactionRequest transactionRequest){
        Transaction transaction = modelMapper.map(transactionRequest, Transaction.class);
        transaction = transactionRepository.save(transaction);
        return modelMapper.map(transaction, TransactionResponse.class);
    }

    /**
     * <p>Updates the Transaction entity by the given Id and TransactionRequest
     * </p>
     * @param id Transaction Id
     * @param transactionRequest updated Transaction Details
     * @throws TransactionNotFoundException When given transaction is not found
     */
    public void updateById(Long id, TransactionRequest transactionRequest) throws TransactionNotFoundException{
        Optional<Transaction> transactionOptional = transactionRepository.findById(id);
        if(!transactionOptional.isPresent())
            throw new TransactionNotFoundException("Transaction not found for Id : "+id);

        Transaction transaction = modelMapper.map(transactionRequest, Transaction.class);
        transaction.setId(id);
        transactionRepository.save(transaction);
    }

    /**
     * <p>Deletes the Transaction entity by the given Id
     * </p>
     * @param id Transaction Id
     * @throws TransactionNotFoundException When given transaction is not found
     */
    public void deleteById(Long id) throws TransactionNotFoundException {
        Optional<Transaction> transactionOptional = transactionRepository.findById(id);
        if(!transactionOptional.isPresent())
            throw new TransactionNotFoundException("Transaction not found for Id : "+id);

        transactionRepository.deleteById(id);
    }

    public ModelMapper getModelMapper() {
        return modelMapper;
    }

    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }
}