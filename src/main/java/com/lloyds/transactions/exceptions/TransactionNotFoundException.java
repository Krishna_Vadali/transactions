package com.lloyds.transactions.exceptions;

public class TransactionNotFoundException extends Exception {

    public TransactionNotFoundException(String errorMessage) {
        super(errorMessage);
    }

}
