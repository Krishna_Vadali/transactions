# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary - Simple Transaction(below json) CRUD Operations - Spring Boot - Rest API - H2 Inmemory DB - Swagger2

{
  "clientId": "9d078bd3-ec13-4728-b484-8dbc2191c32c",
  "cardNumber": "1234-5678-9012-1234",
  "transactionAmount": "100.00",
  "transactionDate": "2020-09-22T18:59:33+0000"
}


### How do I get set up? ###

* git clone https://bitbucket.org/Krishna_Vadali/transactions.git
* Build - maven clean install
* Run - mvn spring-boot:run
* Application starts at http://localhost:8080/transactions/
* Swagger - http://localhost:8080/swagger-ui.html#/
* Health Check - http://localhost:8080/actuator/health
* Jacoco Report after build - target/site/jacoco/index.html
* Generate Java Doc command : mvn javadoc:javadoc  | apidocs available at target/site/apidocs/index.html


### Pending Tasks ###

* Writing request validation tests & Negative Test cases
* Writing more Integration tests
* Code review & refactor if required
* Add better exception handling using @RestControllerAdvice
* Add BDD/Cucumber test cases
* Dockerisation
* Add some more documentation
